# Adafruit Thermal Printer Library
### For Mongoose OS

This is an Arduino library for the Adafruit Thermal Printer.
Pick one up at https://www.adafruit.com/product/597

These printers use TTL serial to communicate, 2 pins are required.

Full tutorial with wiring diagrams, images, etc. is available at:
https://learn.adafruit.com/mini-thermal-receipt-printer

Adafruit invests time and resources providing this open source code.  Please support Adafruit and open-source hardware by purchasing products from Adafruit!

Written by Limor Fried/Ladyada for Adafruit Industries, with contributions from the open source community.
Modified by Bailey Blankenship for use with Mongoose OS. Originally based on Thermal library from bildr.org
MIT license, all text above must be included in any redistribution.
